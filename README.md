R Scripts to reproduce with R the NETSCITY processing chain


[NETSCITY](https://www.irit.fr/netscity) is a geospatial web application allowing to analyse the geography of any set of scientific publications extracted from the Web of Science or Scopus or provided in a custom CSV file.

A paper introducing [NETSCITY](https://www.irit.fr/publis/IRIS/2019_ISSI_MJYC.pdf) was presented at the [ISSI 2019 conference](https://www.issi2019.org/).

The method available in NETSCITY has been developed in the frame of the GEOSCIENCE and NETSCIENCE research programmes.
It is presented on the [GEOSCIMO](http://geoscimo.univ-tlse2.fr/where-does-science-take-place/) website, page [methods](http://geoscimo.univ-tlse2.fr/general-methodology/methods/), and in the publications of the resarch group, page [results and analyses](http://geoscimo.univ-tlse2.fr/results-and-analyses/).

## Selected references:

[Maisonobe, Marion, Jégou, Laurent, Yakimovich, Nikita, & Cabanac, Guillaume. 2019. NETSCITY: a geospatial application to analyse and map world scale production and collaboration data between cities. *ISSI’19: 17th International Conference on Scientometrics and Informetrics*, Rome.](https://www.irit.fr/publis/IRIS/2019_ISSI_MJYC.pdf)  

Maisonobe, Marion, Jégou, Laurent, & Eckert, Denis. 2018. Delineating urban agglomerations across the world: a dataset for studying the spatial distribution of academic research at city level. *Cybergeo: European Journal of Geography*, n° 871. https://doi.org/10.4000/cybergeo.29637  

Maisonobe Marion, Eckert Denis, Grossetti Michel, Jégou Laurent, & Milard Béatrice. 2016. « The world network of scientific collaborations between cities: domestic or international dynamics? ». *Informetrics*. 10 (4): 1025-2036. https://dx.doi.org/10.1016/j.joi.2016.06.002

Grossetti Michel, Eckert Denis, Gingras Yves, Jégou Laurent, Larivière Vincent, Milard, Béatrice. 2013. « Cities and the geographical deconcentration of scientific activity: A multilevel analysis of publications (1987-2007) ». *Urban Studies*. 51 (10) : 2219-2234. https://dx.doi.org/10.1177/0042098013506047